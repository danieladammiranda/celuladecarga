﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BalancaSerial
{
    public partial class Form1 : Form
    {
        delegate void SetTextCallback(string text);
        string RxString;
        public Form1()
        {
            InitializeComponent();
        }

        private void timerCOM_Tick(object sender, EventArgs e)
        {
            int i;
            bool quantDiferente;

            i = 0;
            quantDiferente = false;

            if (cbConexoes.Items.Count == SerialPort.GetPortNames().Length)
            {
                foreach (string s in SerialPort.GetPortNames())
                {
                    if (cbConexoes.Items[i++].Equals(s) == false)
                    {
                        quantDiferente = true;
                    }
                }
            }
            else
            {
                quantDiferente = true;
            }

            if (quantDiferente == false)
            {
                return;
            }

            cbConexoes.Items.Clear();

            foreach (string s in SerialPort.GetPortNames())
            {
                cbConexoes.Items.Add(s);
            }
            cbConexoes.SelectedIndex = 0;
        }

        private void btConectar_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen == false)
            {
                try
                {
                    serialPort1.PortName = cbConexoes.Items[cbConexoes.SelectedIndex].ToString();
                    serialPort1.Open();
                    btIniciar.Enabled = true;
                    timerCOM.Enabled = false;
                }
                catch
                {
                    return;
                }
                if (serialPort1.IsOpen)
                {
                    btConectar.Text = "Desconectar";
                    cbConexoes.Enabled = false;
                }
            }
            else
            {
                try
                {
                    serialPort1.Close();
                    cbConexoes.Enabled = true;
                    btConectar.Text = "Conectar";
                    btIniciar.Text = "Iniciar";
                    btIniciar.Enabled = false;
                    btTara.Enabled = false;
                    timerEnvio.Enabled = false;
                    EditarTexto("--,--");
                    timerCOM.Enabled = true;
                }
                catch
                {
                    return;
                }
            }
        }

        private void btIniciar_Click(object sender, EventArgs e)
        {
            if (timerEnvio.Enabled == false)
            {
                btIniciar.Text = "Parar";
                timerEnvio.Enabled = true;
                btTara.Enabled = true;
            }
            else
            {
                btIniciar.Text = "Iniciar";
                timerEnvio.Enabled = false;
                EditarTexto("--,--");
                btTara.Enabled = false;
            }
        }

        private void timerEnvio_Tick(object sender, EventArgs e)
        {
            char[] bsi = { '0', '1', 'I', '\r', '\n' };
            if (serialPort1.IsOpen == true)
                serialPort1.Write(bsi, 0, bsi.Length);
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                RxString = serialPort1.ReadLine();
            }
            catch
            {
                RxString = "";
            }
            decimal valor = -10000;
            if (RxString.Contains("01I"))
            {
                if (RxString.Contains('+'))
                {
                    string[] parts = RxString.Split('+');
                    try
                    {
                        valor = Convert.ToDecimal(parts[1]) / 1000;
                    }
                    catch
                    {
                        valor = -10000;
                    }
                }
                else if (RxString.Contains('-'))
                {
                    string[] parts = RxString.Split('-');
                    try
                    {
                        valor = (Convert.ToDecimal(parts[1]) * -1) / 1000;
                    }
                    catch
                    {
                        valor = -10000;
                    }
                }

                if (timerEnvio.Enabled == true)
                {
                    if (valor == -10000)
                        EditarTexto("Erro");
                    else
                        EditarTexto(valor.ToString("0.000"));
                }
            }
        }

        private void EditarTexto(string text)
        {
            if (this.lbPeso.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(EditarTexto);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.lbPeso.Text = text;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen == true)
                serialPort1.Close();
        }

        private void btTara_Click(object sender, EventArgs e)
        {
            timerEnvio.Enabled = false;

            char[] bsi = { '0', '1', 'T', '\r', '\n' };
            if (serialPort1.IsOpen == true)
                serialPort1.Write(bsi, 0, bsi.Length);

            timerEnvio.Enabled = true;
        }
    }
}
