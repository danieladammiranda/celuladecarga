﻿namespace BalancaSerial
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btConectar = new System.Windows.Forms.Button();
            this.cbConexoes = new System.Windows.Forms.ComboBox();
            this.lbPeso = new System.Windows.Forms.Label();
            this.lbUnidade = new System.Windows.Forms.Label();
            this.btTara = new System.Windows.Forms.Button();
            this.timerCOM = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.btIniciar = new System.Windows.Forms.Button();
            this.timerEnvio = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btConectar
            // 
            this.btConectar.Location = new System.Drawing.Point(12, 12);
            this.btConectar.Name = "btConectar";
            this.btConectar.Size = new System.Drawing.Size(75, 23);
            this.btConectar.TabIndex = 0;
            this.btConectar.Text = "Conectar";
            this.btConectar.UseVisualStyleBackColor = true;
            this.btConectar.Click += new System.EventHandler(this.btConectar_Click);
            // 
            // cbConexoes
            // 
            this.cbConexoes.FormattingEnabled = true;
            this.cbConexoes.Location = new System.Drawing.Point(94, 13);
            this.cbConexoes.Name = "cbConexoes";
            this.cbConexoes.Size = new System.Drawing.Size(121, 21);
            this.cbConexoes.TabIndex = 1;
            // 
            // lbPeso
            // 
            this.lbPeso.Font = new System.Drawing.Font("Modern No. 20", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPeso.ForeColor = System.Drawing.Color.Red;
            this.lbPeso.Location = new System.Drawing.Point(55, 149);
            this.lbPeso.Name = "lbPeso";
            this.lbPeso.Size = new System.Drawing.Size(224, 81);
            this.lbPeso.TabIndex = 2;
            this.lbPeso.Text = "--,--";
            this.lbPeso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbUnidade
            // 
            this.lbUnidade.Font = new System.Drawing.Font("Modern No. 20", 48F, System.Drawing.FontStyle.Bold);
            this.lbUnidade.ForeColor = System.Drawing.Color.Red;
            this.lbUnidade.Location = new System.Drawing.Point(285, 149);
            this.lbUnidade.Name = "lbUnidade";
            this.lbUnidade.Size = new System.Drawing.Size(159, 81);
            this.lbUnidade.TabIndex = 3;
            this.lbUnidade.Text = "Kg";
            // 
            // btTara
            // 
            this.btTara.Enabled = false;
            this.btTara.Location = new System.Drawing.Point(13, 352);
            this.btTara.Name = "btTara";
            this.btTara.Size = new System.Drawing.Size(75, 23);
            this.btTara.TabIndex = 4;
            this.btTara.Text = "Tara";
            this.btTara.UseVisualStyleBackColor = true;
            this.btTara.Click += new System.EventHandler(this.btTara_Click);
            // 
            // timerCOM
            // 
            this.timerCOM.Enabled = true;
            this.timerCOM.Tick += new System.EventHandler(this.timerCOM_Tick);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // btIniciar
            // 
            this.btIniciar.Enabled = false;
            this.btIniciar.Location = new System.Drawing.Point(13, 42);
            this.btIniciar.Name = "btIniciar";
            this.btIniciar.Size = new System.Drawing.Size(75, 23);
            this.btIniciar.TabIndex = 5;
            this.btIniciar.Text = "Iniciar";
            this.btIniciar.UseVisualStyleBackColor = true;
            this.btIniciar.Click += new System.EventHandler(this.btIniciar_Click);
            // 
            // timerEnvio
            // 
            this.timerEnvio.Interval = 500;
            this.timerEnvio.Tick += new System.EventHandler(this.timerEnvio_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 387);
            this.Controls.Add(this.btIniciar);
            this.Controls.Add(this.btTara);
            this.Controls.Add(this.lbUnidade);
            this.Controls.Add(this.lbPeso);
            this.Controls.Add(this.cbConexoes);
            this.Controls.Add(this.btConectar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btConectar;
        private System.Windows.Forms.ComboBox cbConexoes;
        private System.Windows.Forms.Label lbPeso;
        private System.Windows.Forms.Label lbUnidade;
        private System.Windows.Forms.Button btTara;
        private System.Windows.Forms.Timer timerCOM;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button btIniciar;
        private System.Windows.Forms.Timer timerEnvio;
    }
}

