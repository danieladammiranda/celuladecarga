﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace CelulaDeCarga
{
    public partial class Form1 : Form
    {
        delegate void SetTextCallback(string text);

        public Form1()
        {
            InitializeComponent();
        }

        private void cbxPorta_Click(object sender, EventArgs e)
        {
            foreach (string s in SerialPort.GetPortNames())
            {
                cbxPorta.Items.Clear();
                cbxPorta.Items.Add(s);
            }
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            if (btnAbrir.Text == "ABRIR")
            {
                try
                {
                    if (cbxPorta.SelectedItem.ToString() != "")
                    {
                        serPorta.PortName = cbxPorta.SelectedItem.ToString();
                        serPorta.Open();
                        btnAbrir.Text = "FECHAR";
                        cbxPorta.Enabled = false;
                        tmrTemporizador.Enabled = true;
                    }
                }
                catch
                {
                    MessageBox.Show("Não foi possível abrir a Porta " + serPorta.PortName);
                }
            }
            else
            {
                try
                {
                    serPorta.Close();
                    btnAbrir.Text = "ABRIR";
                    cbxPorta.Enabled = true;
                    tmrTemporizador.Enabled = false;                
                }
                catch
                {
                    MessageBox.Show("Não foi possível fechar a Porta " + serPorta.PortName);
                }
            }
        }

        private void serPorta_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            Console.Write(indata);
            string[] pesos = indata.Split('\n');
            decimal pesoTotal = 0;
            int contador = 0;
            foreach(string s in pesos)
            {
                try
                {
                    string[] parts = s.Split('+');
                    if(parts[0] == "01IS" && parts[1].Contains('\r'))
                    {
                        string peso = parts[1].Replace("\r", "");
                        pesoTotal += Convert.ToDecimal(peso);
                        contador++;
                    }
                }
                catch
                {

                }                
            }  
            
            if(contador > 0)
            {
                pesoTotal = pesoTotal / contador;
                //int kg = Convert.ToInt32(pesoTotal) / 1000;
                //int grama = Convert.ToInt32(pesoTotal) - (kg * 1000);
                // lblPeso.Text = kg.ToString() + "," + grama.ToString();
                //Console.Write(kg.ToString() + "," + grama.ToString() + '\n');
                //SetText(pesoTotal.ToString());
            }                 

        }

        private void SetText(string text)
        {
            if (this.lblPeso.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.lblPeso.Text = text;
            }
        }   

        private void tmrTemporizador_Tick(object sender, EventArgs e)
        {
            char[] bsi = { '0', '1', 'I','\r', '\n' };
            serPorta.Write(bsi,0,bsi.Length);
        }
    }
}
