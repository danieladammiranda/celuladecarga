﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteSerial
{
    public partial class Form1 : Form
    {
        string RxString;
        bool enviando = false;

        public Form1()
        {
            InitializeComponent();
            timerCOM.Enabled = true;
        }

        private void atualizaListaCOMs()
        {
            int i;
            bool quantDiferente;

            i = 0;
            quantDiferente = false;

            if (comboBox1.Items.Count == SerialPort.GetPortNames().Length)
            {
                foreach(string s in SerialPort.GetPortNames())
                {
                    if (comboBox1.Items[i++].Equals(s) == false)
                    {
                        quantDiferente = true;
                    }
                }
            }
            else
            {
                quantDiferente = true;
            }

            if(quantDiferente == false)
            {
                return;
            }

            comboBox1.Items.Clear();

            foreach (string s in SerialPort.GetPortNames())
            {
                comboBox1.Items.Add(s);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void timerCOM_Tick(object sender, EventArgs e)
        {
            atualizaListaCOMs();
        }

        private void btConectar_Click(object sender, EventArgs e)
        {
            if(serialPort1.IsOpen == false)
            {
                try
                {
                    serialPort1.PortName = comboBox1.Items[comboBox1.SelectedIndex].ToString();
                    serialPort1.Open();
                }
                catch
                {
                    return;
                }
                if (serialPort1.IsOpen)
                {
                    btConectar.Text = "Desconectar";
                    comboBox1.Enabled = false;
                }
            }
            else
            {
                try
                {
                    serialPort1.Close();
                    comboBox1.Enabled = true;
                    btConectar.Text = "Conectar";
                }
                catch
                {
                    return;
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen == true)
                serialPort1.Close();
        }

        private void btEnviar_Click(object sender, EventArgs e)
        {
            string texto = textBoxEnviar.Text;
            char[] bsi = new char[texto.Length + 2];
            for (int i = 0; i < texto.Length; i++)
                bsi[i] = texto[i];
            bsi[texto.Length] = '\r';
            bsi[texto.Length + 1] = '\n';
            if (serialPort1.IsOpen == true)
                serialPort1.Write(bsi, 0, bsi.Length);
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            RxString = serialPort1.ReadExisting();
            this.Invoke(new EventHandler(trataDadoRecebido));
        }

        private void trataDadoRecebido(object sender, EventArgs e)
        {
            textBoxReceber.AppendText(RxString);
        }

        private void buttonContinuo_Click(object sender, EventArgs e)
        {
            if (!enviando)
            {
                enviando = true;
                buttonContinuo.Text = "Parar";
                timerEnvio.Enabled = true;
            }
            else
            {
                enviando = false;
                buttonContinuo.Text = "Envia Contínuo";
                timerEnvio.Enabled = false;
            }
        }

        private void timerEnvio_Tick(object sender, EventArgs e)
        {
            string texto = textBoxEnviar.Text;
            char[] bsi = new char[texto.Length + 2];
            for (int i = 0; i < texto.Length; i++)
                bsi[i] = texto[i];
            bsi[texto.Length] = '\r';
            bsi[texto.Length + 1] = '\n';
            if (serialPort1.IsOpen == true)
                serialPort1.Write(bsi, 0, bsi.Length);
        }
    }
}
