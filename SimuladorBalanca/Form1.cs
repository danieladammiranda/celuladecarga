﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorBalanca
{
    public partial class Form1 : Form
    {
        delegate void SetTextCallback(string text);
        private static readonly Socket clientSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private byte[] buffer;
        private MemoryStream ms = new MemoryStream();
        private BinaryFormatter bf = new BinaryFormatter();

        private const int PORT = 2000;

        private static Mutex mutex = new Mutex();
        private static decimal valorPeso;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonIniciar_Click(object sender, EventArgs e)
        {
            bool conectar = ConnectToServer();
            if (conectar)
            {
                buttonIniciar.Enabled = false;
                buttonTara.Enabled = true;
                timerEnvio.Enabled = true;
                valorPeso = 0;
                timerValor.Enabled = true;
                buffer = new byte[clientSocket.ReceiveBufferSize];
                clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            else
            {
                MessageBox.Show("Não foi possível conectar com o serviço");
            }
        }

        private bool ConnectToServer()
        {
            int attempts = 0;
            bool connected = false;

            while (!clientSocket.Connected && attempts < 10)
            {
                try
                {
                    attempts++;
                    clientSocket.Connect(IPAddress.Loopback, PORT);
                    connected = true;
                }
                catch (SocketException) { }
            }

            return connected;
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                int received = clientSocket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }

                string message = Encoding.ASCII.GetString(buffer);

                mutex.WaitOne();
                valorPeso = Convert.ToDecimal(message);
                mutex.ReleaseMutex();

                // Start receiving data again.
                clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            // Avoid Pokemon exception handling in cases like these.
            catch { }
        }

        private void timerEnvio_Tick(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("1");
            clientSocket.Send(buffer, 0, buffer.Length, SocketFlags.None);
        }



        private void timerValor_Tick(object sender, EventArgs e)
        {
            mutex.WaitOne();
            EditarTexto(valorPeso.ToString());
            mutex.ReleaseMutex();
        }

        private void EditarTexto(string text)
        {
            if (this.labelPeso.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(EditarTexto);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.labelPeso.Text = text;
            }
        }
    }
}
