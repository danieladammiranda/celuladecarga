﻿namespace SimuladorBalanca
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonIniciar = new System.Windows.Forms.Button();
            this.labelPeso = new System.Windows.Forms.Label();
            this.buttonTara = new System.Windows.Forms.Button();
            this.timerEnvio = new System.Windows.Forms.Timer(this.components);
            this.labelUnidade = new System.Windows.Forms.Label();
            this.timerValor = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonIniciar
            // 
            this.buttonIniciar.Location = new System.Drawing.Point(13, 13);
            this.buttonIniciar.Name = "buttonIniciar";
            this.buttonIniciar.Size = new System.Drawing.Size(75, 23);
            this.buttonIniciar.TabIndex = 0;
            this.buttonIniciar.Text = "Iniciar";
            this.buttonIniciar.UseVisualStyleBackColor = true;
            this.buttonIniciar.Click += new System.EventHandler(this.buttonIniciar_Click);
            // 
            // labelPeso
            // 
            this.labelPeso.Font = new System.Drawing.Font("Monotype Corsiva", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeso.ForeColor = System.Drawing.Color.Red;
            this.labelPeso.Location = new System.Drawing.Point(50, 125);
            this.labelPeso.Name = "labelPeso";
            this.labelPeso.Size = new System.Drawing.Size(223, 87);
            this.labelPeso.TabIndex = 1;
            this.labelPeso.Text = "--,--";
            this.labelPeso.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonTara
            // 
            this.buttonTara.Enabled = false;
            this.buttonTara.Location = new System.Drawing.Point(13, 305);
            this.buttonTara.Name = "buttonTara";
            this.buttonTara.Size = new System.Drawing.Size(75, 23);
            this.buttonTara.TabIndex = 2;
            this.buttonTara.Text = "Tara";
            this.buttonTara.UseVisualStyleBackColor = true;
            // 
            // timerEnvio
            // 
            this.timerEnvio.Interval = 200;
            this.timerEnvio.Tick += new System.EventHandler(this.timerEnvio_Tick);
            // 
            // labelUnidade
            // 
            this.labelUnidade.Font = new System.Drawing.Font("Monotype Corsiva", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.labelUnidade.ForeColor = System.Drawing.Color.Red;
            this.labelUnidade.Location = new System.Drawing.Point(279, 125);
            this.labelUnidade.Name = "labelUnidade";
            this.labelUnidade.Size = new System.Drawing.Size(125, 87);
            this.labelUnidade.TabIndex = 3;
            this.labelUnidade.Text = "Kg";
            // 
            // timerValor
            // 
            this.timerValor.Interval = 500;
            this.timerValor.Tick += new System.EventHandler(this.timerValor_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 340);
            this.Controls.Add(this.labelUnidade);
            this.Controls.Add(this.buttonTara);
            this.Controls.Add(this.labelPeso);
            this.Controls.Add(this.buttonIniciar);
            this.Name = "Form1";
            this.Text = "Balança";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonIniciar;
        private System.Windows.Forms.Label labelPeso;
        private System.Windows.Forms.Button buttonTara;
        private System.Windows.Forms.Timer timerEnvio;
        private System.Windows.Forms.Label labelUnidade;
        private System.Windows.Forms.Timer timerValor;
    }
}

